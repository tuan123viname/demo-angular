import { Component, OnInit,Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {products} from '../../services/product.service';
@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  product;
  constructor(  private route: ActivatedRoute,) { 
  
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
     const id=(+params.get('id'));
     this.product=products[id-1];
  })

  }
}
