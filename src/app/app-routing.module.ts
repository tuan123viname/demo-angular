import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProductItemComponent} from './product-item/product-item.component';
import {HomePageComponent} from './home-page/home-page.component';
import {ProductListComponent} from './product-list/product-list.component';
import {ProductDetailComponent} from './product-detail/product-detail.component';
import { products } from 'src/services/product.service';
const routes: Routes = [
  {path:'',component:HomePageComponent},
  {path:'list-product',component:ProductListComponent},
  {path:'list-product/:id',component:ProductDetailComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
